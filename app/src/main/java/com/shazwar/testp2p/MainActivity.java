package com.shazwar.testp2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Proxy;
import android.net.ProxyInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.shazwar.wifidirector.WifiDirectManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    ListView availableProxyList;
    Button discover;
    Button host;
    Button stopHost;
    Context mContext;
    public static final String TAG = "TestActivity";

    private HashMap availableServices = new HashMap<String , HashMap>();
    private ArrayList<String> serviceNames = new ArrayList<String>();
    private ArrayAdapter<String> serviceArrayAdapter;

    private BroadcastReceiver servicesReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WifiDirectManager.initialize(this);
        mContext = getBaseContext();
        discover = (Button)(findViewById(R.id.button3));
        host = (Button)(findViewById(R.id.host));
        stopHost = (Button)(findViewById(R.id.stop_host));
        availableProxyList = (ListView) findViewById(R.id.proxyList);
        serviceArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, serviceNames);
        availableProxyList.setAdapter(serviceArrayAdapter);

        availableProxyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String hostName = serviceNames.get(position);
                Toast.makeText(mContext, String.format("Attempting Connection to host: %s", hostName), Toast.LENGTH_SHORT).show();
                connectToProxy(hostName);
            }
        });

        servicesReceiver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent i) {
                Log.d(TAG, "available services broadcast received: " + i.getStringExtra("data"));

                if (availableServices != null){
                    try {
                        JSONObject mServices = new JSONObject(i.getStringExtra("data"));
                        Iterator<String> serviceKeys = mServices.keys();
                        while (serviceKeys.hasNext()){
                            String key = serviceKeys.next();
                            if (key.contains("proxy") == true){
                                JSONObject devices = (JSONObject) mServices.get(key);
                                Iterator<String> deviceKeys = devices.keys();
                                while(deviceKeys.hasNext()){
                                    String device_id = deviceKeys.next();
                                    JSONObject val = (JSONObject) devices.get(device_id);
                                    if (!availableServices.containsKey(val.get("ssid"))){
                                        serviceNames.add((String)val.get("ssid"));
                                    }
                                    availableServices.put(val.get("ssid"), val);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }finally{
                        Log.d(TAG, "proxy services: " + Arrays.toString(serviceNames.toArray()) );
                        serviceArrayAdapter.notifyDataSetChanged();

                    }
                }

            }
        };

        discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "pressed discover");
                Intent i = new Intent(mContext, WifiDirectManager.class);
                i.setAction("com.shazwar.wifidirector.action.findservice");
                i.putExtra("com.shazwar.wifidirector.extra.APP_NAME","app2");
                startService(i);
                //
            }
        });

        host.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "pressed host");
                Intent i = new Intent(mContext, WifiDirectManager.class);
                i.setAction(WifiDirectManager.ACTION_HOST);
                i.putExtra(WifiDirectManager.EX_APP_NAME,"app2");
                startService(i);
                //
            }
        });

        stopHost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "pressed stop host");
                Intent i = new Intent(mContext, WifiDirectManager.class);
                i.setAction(WifiDirectManager.ACTION_STOP_HOST);
                startService(i);
                //
            }
        });



    }

    private void connectToProxy(String name){
        Thread t = new Thread(new ProxyConnector(getApplicationContext(), name));
        t.start();
    }

    @Override
    protected void onResume() {
        IntentFilter servicesFilter = new IntentFilter();
        servicesFilter.addAction(WifiDirectManager.BROADCAST_AVAILABLE_SERVICES);
        registerReceiver(servicesReceiver, servicesFilter);
        super.onResume();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(servicesReceiver);
        super.onStop();
    }


    public class ProxyConnector implements Runnable{
        String name;
        Context context;
        public ProxyConnector(Context mContext, String hostName){
            name = hostName;
            context = mContext;
        }
        @Override
        public void run() {
            try {
                JSONObject details = (JSONObject) availableServices.get(name);
                Log.d(TAG, "service details: " + details.toString());
                WifiManager wifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);

                WifiConfiguration conf = new WifiConfiguration();
                String ssid = (String) details.get("ssid");
                String ip = (String) details.get("ip");
                int port = Integer.parseInt((String) details.get("port"));

                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                for( WifiConfiguration i : list ) {
                    if(i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                        Log.d(TAG, "Found old network config for: " + ssid);
                        wifiManager.removeNetwork(i.networkId);
                    }
                }

                conf.SSID = "\"" + ssid + "\"";
                conf.preSharedKey = "\""+ (String) details.get("pw") +"\"";


                //Set Proxy by Reflection
                Class proxySettings = Class.forName("android.net.IpConfiguration$ProxySettings");

                Class[] setProxyParams = new Class[2];
                setProxyParams[0] = proxySettings;
                setProxyParams[1] = ProxyInfo.class;

                Method setProxy = conf.getClass().getDeclaredMethod("setProxy", setProxyParams);
                setProxy.setAccessible(true);

                ProxyInfo desiredProxy = ProxyInfo.buildDirectProxy(ip, port);

                Object[] methodParams = new Object[2];
                methodParams[0] = Enum.valueOf(proxySettings, "STATIC");
                methodParams[1] = desiredProxy;

                setProxy.invoke(conf, methodParams);




                wifiManager.addNetwork(conf);
                list = wifiManager.getConfiguredNetworks();
                for( WifiConfiguration i : list ) {
                    if(i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                        Log.d(TAG, "Found network : " + ssid);
                        wifiManager.disconnect();
                        wifiManager.enableNetwork(i.networkId, true);
                        wifiManager.reconnect();

                        break;
                    }
                }

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(context, "Done!", Toast.LENGTH_SHORT).show();
                    }
                });

            }catch (Exception e){

            }


        }

    };



}

